#include "Animal.h"
#pragma once

class Dog: public Animal
{
private:
	bool _sheddHair;
public:
	Dog(float weight, bool sheddHair);
	void sayHey() const { std::cout << "Hello, I'm a dog" << std::endl; }
	~Dog();
};


