#include <string>
#include <iostream>
#pragma once
class Animal
{
protected:
	float _weight;


public: 
	Animal(float weight);
	float getWeight() const { return _weight; }
	void sayHey() const { std::cout << "Hello" << std::endl; }
	~Animal();

};

