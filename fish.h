#include "Animal.h"
#pragma once
class Fish: public Animal
{
private:
	bool _scale;
public:
	Fish(float weight, bool scale);
	float getWeight() const { return _weight; }
	void sayHey() const { std::cout << "Hello, I'm a fish" << std::endl; }
};



